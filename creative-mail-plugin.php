<?php

use CreativeMail\CreativeMail;

/**
 * Plugin Name: Creativ.eMail by Constant Contact
 * Plugin URI: https://bitbucket.org/creativemail/creativemail-for-wordpress/src
 * Description: Power your WooCommerce Store or WordPress Blog with simple & free email marketing from Constant Contact. With the official Creativ.eMail for WooCommerce plugin, your products, blog posts, images and store links are automatically included as rich shoppable email marketing content for your customers. Our included CRM also intelligently pulls in and identifies your WordPress site contacts and WooCommerce store customers. That makes it easy to build audiences and send targeted customer campaigns. Get free email marketing, 98% deliverability, and Constant Contact rock solid reliability all without ever needing to leave your WP Admin.
 * Author: Constant Contact
 * Version: 1.0-{BUILD}
 * Author URI: https://www.constantcontact.com
 */

function _load_ce_plugin() {

    global $creativemail;

    if($creativemail != null){
        return true;
    }

    define('CE_PLUGIN_DIR', __DIR__ . '/');
    define('CE_PLUGIN_URL', plugin_dir_url(__FILE__) . '/');
    define('CE_PLUGIN_VERSION', '1.0-{BUILD}');
    define('CE_INSTANCE_UUID_KEY', 'ce4wp_instance_uuid');
    define('CE_INSTANCE_ID_KEY', 'ce4wp_instance_id');
    define('CE_INSTANCE_API_KEY_KEY', 'ce4wp_instance_api_key');
	define('CE_ENCRYPTION_KEY_KEY', 'ce4wp_encryption_key');
    define('CE_CONNECTED_ACCOUNT_ID', 'ce4wp_connected_account_id');
    define('CE_ACTIVATED_PLUGINS', 'ce4wp_activated_plugins');
    define('CE_SYNCHRONIZE_ACTION', 'ce4wp_synchronize_contacts');
    define('CE_APP_GATEWAY_URL', '{GATEWAY_URL}');
    define('CE_APP_URL', '{APP_URL}');
    define('CE_ENVIRONMENT', '{ENV}');

    // Load all the required files
    if (file_exists(__DIR__ . '/vendor/autoload.php')) {
        require_once __DIR__ . '/vendor/autoload.php';
    }

    $creativemail = CreativeMail::get_instance();
    $creativemail->add_hooks();

    return true;
}

add_action('plugins_loaded', '_load_ce_plugin', 10);
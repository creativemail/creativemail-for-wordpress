<?php

namespace CreativeMail\Integrations;

/**
 * Class Integration
 *
 * Describes an integration between Creativ.eMail and WordPress.
 *
 * @package CreativeMail\Integrations
 */
class Integration
{
    private $name;
    private $class;
    private $integrationHandler;
    private $slug;

	/**
	 * Integration constructor.
	 *
	 * @param $slug string The slug that you want to use for this integration.
	 * @param $name string The display name of the plugin
	 * @param $class string The path the the plugin class that should be used to check if the plugin required for this integration is installed.
	 * @param $integration_handler string The name of the class that should be instantiated when this integration gets activated.
	 */
    public function __construct($slug, $name, $class, $integration_handler)
    {
        $this->slug = $slug;
        $this->name = $name;
        $this->class = $class;
        $this->integrationHandler = $integration_handler;
    }

	/**
	 * Gets the slug assigned to this integration.
	 * @return string
	 */
    public function get_slug() {
        return $this->slug;
    }

	/**
	 * Gets the display name assigned to this integration.
	 * @return string
	 */
    public function get_name() {
        return $this->name;
    }

	/**
	 * Gets the path to the main class of the plugin that is required for this integration.
	 * @return string
	 */
    public function get_class() {
        return $this->class;
    }

	/**
	 * Gets the name of the class that should be instantiated when activating this integration.
	 * @return string
	 */
    public function get_integration_handler() {
        return $this->integrationHandler;
    }
}
<?php


namespace CreativeMail\Managers;

use CreativeMail\CreativeMail;
use CreativeMail\Helpers\OptionsHelper;
use CreativeMail\Modules\WooCommerce\Models\WCProductModel;
use CreativeMail\Modules\Blog\Models\BlogInformation;
use CreativeMail\Modules\Blog\Models\BlogPost;
use CreativeMail\Modules\Blog\Models\BlogAttachment;
use WP_Error;
use WP_REST_Response;

/**
 * Class ApiManager
 * @package CreativeMail\Managers
 */
class ApiManager
{
    const api_namespace = "creativemail/v1";

    function __construct()
    {
    }

    /**
     * Will add all the hooks that are required to setup our plugin API.
     */
    public function add_hooks() {

        add_action( 'rest_api_init', array($this, 'add_rest_endpoints'));

    }

    public function add_rest_endpoints() {
        // Add the endpoint to handle the callback
        $routes = array (
            array (
                'path'              => '/callback',
                'methods'           => 'POST',
                'callback'          => array(CreativeMail::get_instance()->get_instance_manager(), 'handle_callback'),
                'require_wp_admin'  => false,
                'require_api_key'   => false
            ),
            array (
                'path'              => '/available_plugins',
                'methods'           => 'GET',
                'callback'          => function() {
                    $result = array();
                    $plugins = CreativeMail::get_instance()->get_integration_manager()->get_active_plugins();
                    foreach ($plugins as $activePlugin) {
                      array_push($result, array(
                          'name' => $activePlugin->get_name(),
                          'slug' => $activePlugin->get_slug()
                      ));
                    }
                    
                    return new WP_REST_Response($result, 200);
                }
            ),
            array (
                'path'              => '/available_plugins',
                'methods'           => 'POST',
                'callback'          => function($request) {
                    CreativeMail::get_instance()->get_integration_manager()->set_activated_plugins(json_decode($request->get_body()));
                }
            ),
            array (
              'path'              => '/synchronize',
              'methods'           => 'POST',
              'require_wp_admin'  => false,
              'require_api_key'   => true,
              'callback'          => function() {
                do_action(CE_SYNCHRONIZE_ACTION);
                return new WP_REST_Response(null, 200);
              }
            ),
            array (
                'path'              => '/wc_products',
                'methods'           => 'GET',
                'require_wp_admin'  => false,
                'require_api_key'   => true,
                'callback'          => function() {
                    $productData = array();
                    if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))))
                    {                        
                        // Get 25 most recent products
                        $products = wc_get_products(array(
                            'limit' => 25,
                        ));
                        foreach ($products as $product) {
                          array_push($productData, new WCProductModel($product->get_data()));
                        }
                    }
                    return new WP_REST_Response($productData, 200);
                }
            ),
            array (
                'path'              => '/blog_information',
                'methods'           => 'GET',
                'require_wp_admin'  => false,
                'require_api_key'   => true,
                'callback'          => function() {
                    return new WP_REST_Response(new BlogInformation(), 200);
                }
            ),
            array (
                'path'              => '/wp_posts',
                'methods'           => 'GET',
                'require_wp_admin'  => false,
                'require_api_key'   => true,
                'callback'          => function() {
                    $postData = array();
  
                    $posts = get_posts(array(
                      'posts_per_page' => -1
                    ));
  
                    foreach ($posts as $post)
                    {
                      array_push($postData, new BlogPost($post));
                    }
                    
                    return new WP_REST_Response($postData, 200);
                }
            ),
            array (
                'path'              => '/images',
                'methods'           => 'GET',
                'require_wp_admin'  => false,
                'require_api_key'   => true,
                'callback'          => function() {
                    $attachmentData = array();
                    $attachments = get_posts(array(
                      'post_type' => 'attachment',
                      'post_mime_type' => 'image',
                      'post_status' => 'inherit',
                      'posts_per_page' => -1
                    ));
  
                    foreach ($attachments as $attachment)
                    {
                      array_push($attachmentData, new BlogAttachment($attachment));
                    }
                    
                    return new WP_REST_Response($attachmentData, 200);
                }
            )
        );

        foreach ($routes as $route) {
            $this->register_route($route);
        }
    }

    /**
     * Registers a route to the WP Rest endpoints for this plugin.
     * @param array $route
     */
    private function register_route(array $route) {

        // Make sure the route is valid
        $path = $route['path'];
        $methods = $route['methods'];
        $callback = $route['callback'];
        $require_wp_admin = $route['require_wp_admin'] ?? false;
        $require_api_key = $route['require_api_key'] ?? true;

        // Make sure we at least have a path
        if (empty($path)) return;

        // Skip the route if we are not in admin mode and the route should only be available in admin mode.
        $is_admin = current_user_can('administrator');
        if($require_wp_admin === true && !$is_admin) {
            return;
        }

        // If we don't have a method, assume it is GET
        if(empty($methods)) {
            $methods = 'GET';
        }

        $arguments = array(
            'methods'               => $methods,
            'callback'              => $callback
        );

        $permission_callback = null;
        if ($require_api_key === true) {
            $arguments['permission_callback'] = array($this, 'validate_api_key');
        }

        register_rest_route(self::api_namespace, $path, $arguments);

    }

    /**
    * Will validate if the request is presenting a valid API key.
    * @param $request
    * @return bool|WP_Error
    */
    public function validate_api_key($request) {

        $key = OptionsHelper::get_instance_api_key();
        $apiKey = $request->get_header("x-api-key");
        if ($apiKey === $key) {
            return true;
        }

        return new WP_Error( 'rest_forbidden', __( 'Sorry, you are not allowed to do that.' ), array( 'status' => 401 ) );
    }

}
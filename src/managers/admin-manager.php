<?php

namespace CreativeMail\Managers;

use CreativeMail\Helpers\EnvironmentHelper;
use CreativeMail\Helpers\OptionsHelper;
use CreativeMail\Helpers\SsoHelper;
use Exception;

/**
 * The AdminManager will manage the admin section of the plugin.
 *
 * @ignore
 */
class AdminManager
{

    protected $instance_name;
    protected $instance_uuid;
    protected $instance_id;
    protected $instance_url;
    protected $instance_callback_url;
    protected $dashboard_url;

	/**
	 * AdminManager constructor.
	 */
    public function __construct()
    {
        $this->instance_name = rawurlencode(get_bloginfo('name'));
        $this->instance_uuid = OptionsHelper::get_instance_uuid();
        $this->instance_id = OptionsHelper::get_instance_id();
        $this->instance_url = rawurlencode(get_bloginfo('wpurl'));
        $this->instance_callback_url = rawurlencode(get_bloginfo('wpurl') . '?rest_route=/creativemail/v1/callback');
        $this->dashboard_url = EnvironmentHelper::get_app_url() . 'marketing/dashboard?wp_site_name=' . $this->instance_name
                               . '&wp_site_uuid=' . $this->instance_uuid
                               . '&wp_callback_url=' . $this->instance_callback_url
                               . '&wp_instance_url=' . $this->instance_url
                               . '&wp_version=' . get_bloginfo('version')
                               . '&plugin_version=' . CE_PLUGIN_VERSION;
	}

    /**
     * Will register all the hooks for the admin portion of the plugin.
     */
    public function add_hooks()
    {
        add_action('admin_menu', array( $this, 'build_menu' ));
        add_action('admin_enqueue_scripts', array( $this, 'add_assets' ));
    }

    /**
     * Will add all the required assets for the admin portion of the plugin.
     */
    public function add_assets()
    {
        wp_register_style('ce_admin', CE_PLUGIN_URL . 'assets/css/admin.css', null, CE_PLUGIN_VERSION);
        wp_enqueue_style( 'ce_admin');
    }

    /**
     * Will build the menu for WP-Admin
     */
    public function build_menu()
    {
        // Did the user complete the entire setup?
        $main_action = OptionsHelper::get_instance_id() !== null
            ? array( $this, 'show_dashboard' )
            : array( $this, 'show_setup' );

        // Create the root menu item
        add_menu_page('Creativ.eMail', esc_html__('Creativ.eMail'), 'manage_options', 'creativemail', $main_action, CE_PLUGIN_URL . 'assets/images/icon.png');

        $sub_actions = array(
            array(
                'title'    => esc_html__( 'Settings', 'ce4wp' ),
                'text'     => 'Settings',
                'slug'     => 'creativemail_settings',
                'callback' => array( $this, 'show_settings_page' )
            )
        );

        foreach ($sub_actions as $sub_action) {
            add_submenu_page( 'creativemail', 'Creativ.eMail - ' . $sub_action['title'], $sub_action['text'], 'manage_options', $sub_action['slug'], $sub_action['callback']);
        }
    }

    /**
     * Renders the onboarding flow.
     */
    public function show_setup()
    {
        require CE_PLUGIN_DIR . 'src/views/onboarding.php';
    }

    /**
     * Renders the Creativ.eMail dashboard when the site is connected to an account.
     */
    public function show_dashboard()
    {
        // If all the three values are available, we can use the SSO flow
        $instance_id = OptionsHelper::get_instance_id();
        $instance_api_key = OptionsHelper::get_instance_api_key();
        $connected_account_id = OptionsHelper::get_connected_account_id();

        if (isset($instance_id) && isset($instance_api_key) && isset($connected_account_id)) {
            try {
                $sso_link = SsoHelper::generate_sso_link($instance_id, $instance_api_key, $connected_account_id);
                if(isset($sso_link)){
                    $this->dashboard_url = $sso_link;
                }
            }
            catch(Exception $ex) { }
        }

        require CE_PLUGIN_DIR . 'src/views/dashboard.php';
    }

    /**
     * Renders the settings page for this plugin.
     */
    public function show_settings_page()
    {
        require CE_PLUGIN_DIR . 'src/views/settings.php';
    }
}
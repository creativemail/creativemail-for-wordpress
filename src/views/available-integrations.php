<?php

use CreativeMail\CreativeMail;

$supported_integrations = CreativeMail::get_instance()->get_integration_manager()->get_supported_integrations();

?>

<p>
    We couldn't find any plugins that we support. <br/>
    In order to help you sync your contacts to Creativ.eMail we have build integrations with the following plugins:
</p>
<ul>
	<?php
		foreach ($supported_integrations as $supported_integration) {
			echo '<li>- ' . $supported_integration->get_name() . '</li>';
		}
	?>
</ul>

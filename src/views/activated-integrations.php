<?php

use CreativeMail\CreativeMail;

$available_integrations = CreativeMail::get_instance()->get_integration_manager()->get_active_plugins();
$activated_integrations = CreativeMail::get_instance()->get_integration_manager()->get_activated_integrations();

?>


<p>We will synchronise your contacts from the following plugins:</p>


<form name="plugins" action="" method="post">
	<input type="hidden" name="action" value="activated_plugins" />
	<ul>
		<?php

		foreach ($available_integrations as $available_integration){

			$active = in_array($available_integration, $activated_integrations);

			echo $active === true
				? '<li><label class="ce-checkbox"><input type="checkbox" name="'. esc_attr($available_integration->get_slug()) . '" checked ><span>' . esc_html($available_integration->get_name()) . '</span></label></li>'
				: '<li><label class="ce-checkbox"><input type="checkbox" name="'. esc_attr($available_integration->get_slug()) . '" ><span>' . esc_html($available_integration->get_name()) . '</span></label></li>';
		}

		?>
	</ul>

	<div class="ce-kvp">
        <input name="save_button" type="submit" class="ce-button-text-primary ce-right" id="save-activated-plugins" value="Save" />
    </div>
</form>

<?php
	use CreativeMail\Helpers\EnvironmentHelper;

	$redirectUrl = EnvironmentHelper::get_app_gateway_url('wordpress/v1.0/instances/open?clearSession=true&redirectUrl=');
	$onboardingUrl = EnvironmentHelper::get_app_url() . 'marketing/onboarding?wp_site_name=' . $this->instance_name
                     . '&wp_site_uuid=' . $this->instance_uuid
                     . '&wp_callback_url=' . $this->instance_callback_url
                     . '&wp_instance_url=' . $this->instance_url
                     . '&wp_version=' . get_bloginfo('version')
                     . '&plugin_version=' . CE_PLUGIN_VERSION;
?>

<iframe id="ce-onboarding" src="<?php echo esc_url($redirectUrl . rawurlencode($onboardingUrl)) ?>" class="ce-iframe">
</iframe>

<script type="application/javascript">

    function adjustHeight() {
        const height = window.innerHeight - document.getElementById('wpadminbar').clientHeight;
        const iframe = document.getElementById('ce-onboarding');
        iframe.height = height + 'px';
    }

    function onWindowResize() {
        adjustHeight();
    }

    window.addEventListener('resize', onWindowResize);
    adjustHeight();

</script>
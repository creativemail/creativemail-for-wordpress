<?php

    use CreativeMail\CreativeMail;
    use CreativeMail\Helpers\EnvironmentHelper;
    use CreativeMail\Helpers\OptionsHelper;

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        if($_POST['action'] === 'disconnect') {
            OptionsHelper::clear_options(true);
        }

        if($_POST['action'] === 'activated_plugins') {

            $activated_plugins = array();
            $keys = array_keys($_POST);
            foreach ($keys as $key) {

                if($_POST[$key] === 'on'){
                    array_push($activated_plugins, $key);
                }

            }

            CreativeMail::get_instance()->get_integration_manager()->set_activated_plugins($activated_plugins);
        }
    }

    $contact_sync_available = !empty(CreativeMail::get_instance()->get_integration_manager()->get_active_plugins());

?>

<div class="ce-admin-wrapper">
    <header class="ce-header">
        <div class="ce-logo"></div>
    </header>
    <div class="ce-container">

            <h2>Settings</h2>
            <div class="ce-card" style="display: <?php echo !empty($this->instance_id) ? 'block' : 'none' ?>">

                <h4>Contact Synchronization</h4>

                <?php
                if($contact_sync_available){
                    include 'activated-integrations.php';
                }
                else {
                    include 'available-integrations.php';
                }
                ?>

            </div>

            <div class="ce-card">
                <h4>Creativ.eMail for WordPress</h4>

                <?php
                    if(EnvironmentHelper::is_test_environment()) {
                        include 'settings-internal.php';
                    }
                ?>

                <div class="ce-kvp">
                    <h6 style="display: <?php echo OptionsHelper::get_instance_id() !== null ? 'block' : 'none' ?>">Your WordPress instance is connected to your Creativ.eMail account. If you would like to unlink your WordPress instance from your account, please click the 'Unlink' button below.</h6>
                </div>

                <div class="ce-kvp">
                    <form name="disconnect" action="" method="post">
                        <input type="hidden" name="action" value="disconnect" />
                        <input name="disconnect_button" type="submit" class="ce-button-text-primary destructive ce-right" id="disconnect-instance" value="Unlink" />
                    </form>
                </div>
            </div>
    </div>
</div>

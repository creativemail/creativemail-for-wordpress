<?php

use CreativeMail\Helpers\EnvironmentHelper;

$redirectUrl = EnvironmentHelper::get_app_gateway_url('wordpress/v1.0/instances/open?redirectUrl=');
?>

<iframe id="ce-dashboard" src="<?php echo esc_url($redirectUrl . rawurlencode($this->dashboard_url)) ?>" class="ce-iframe">
</iframe>

<script type="application/javascript">

    function adjustHeight() {
        const height = window.innerHeight - document.getElementById('wpadminbar').clientHeight;
        const iframe = document.getElementById('ce-dashboard');
        iframe.height = height + 'px';
    }

    function onWindowResize() {
        adjustHeight();
    }

    window.addEventListener('resize', onWindowResize);
    adjustHeight();

</script>
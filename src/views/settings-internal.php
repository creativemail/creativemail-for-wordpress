<?php
use CreativeMail\Helpers\EnvironmentHelper;
?>

<div class="ce-kvp">
    <h6>Instance UUID</h6>
    <h5><?php echo esc_html($this->instance_uuid) ?></h5>
</div>

<div class="ce-kvp">
	<h6>Instance Id</h6>
	<h5><?php echo esc_html($this->instance_id) ?></h5>
</div>

<div class="ce-kvp">
	<h6>Environment</h6>
	<h5><?php echo esc_html(EnvironmentHelper::get_environment()) ?></h5>
</div>

<div class="ce-kvp">
	<h6>App</h6>
	<h5><?php echo esc_js(EnvironmentHelper::get_app_url()) ?></h5>
</div>

<div class="ce-kvp">
	<h6>App Gateway</h6>
	<h5><?php echo esc_js(EnvironmentHelper::get_app_gateway_url()) ?></h5>
</div>

<?php

namespace CreativeMail\Constants;

class EnvironmentNames
{
    const DEVELOPMENT = 'DEVELOP';
    const PRODUCTION = 'PRODUCTION';
}
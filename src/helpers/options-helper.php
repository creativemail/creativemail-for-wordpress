<?php

namespace CreativeMail\Helpers;

/**
 * Class CE4WP_OptionsHelper
 * Exposes a wrapper around all the options that we register within the plugin.
 * @package CreativeMail\Helpers
 * @access private
 */
class OptionsHelper
{
    /**
     * Gets the generated unique id for this WP instance, or will generate a new unique id if none is present.
     * @return string
     */
    public static function get_instance_uuid()
    {

        // Do we already have a UUID?
        $instanceUuid = get_option(CE_INSTANCE_UUID_KEY, null);
        if ($instanceUuid === null) {

            // Just generate one and store it
            $instanceUuid = uniqid();
            add_option(CE_INSTANCE_UUID_KEY, $instanceUuid);
        }

        return $instanceUuid;
    }

    /**
     * Gets the assigned instance id.
     * @return int|null
     */
    public static function get_instance_id()
    {
        return get_option(CE_INSTANCE_ID_KEY, null);
    }

    /**
     * Sets the assigned instance id that is generated when connecting this WP instance to the Creativ.eMail account.
     * @param $value int
     */
    public static function set_instance_id($value)
    {
        add_option(CE_INSTANCE_ID_KEY, $value);
    }

    /**
     * Gets the id of the account that is connected to the combination of this WP unique id and Creativ.eMail account id.
     * @return int|null
     */
    public static function get_connected_account_id()
    {
        return get_option(CE_CONNECTED_ACCOUNT_ID, null);
    }

    /**
     * Sets the id of the account that is connected to the combination of this WP unique id and Creativ.eMail account id.
     * @param $value int
     */
    public static function set_connected_account_id($value)
    {
        add_option(CE_CONNECTED_ACCOUNT_ID, $value);
    }

    /**
     * Gets the API key that can be used to interact with the Creativ.eMail platform.
     * @return string|null
     */
    public static function get_instance_api_key()
    {
        return EncryptionHelper::get_option(CE_INSTANCE_API_KEY_KEY, null);
    }

	/**
	 * Sets the API key that can be used to interact with the Creativ.eMail platform.
	 *
	 * @param $value string
	 *
	 * @throws \Defuse\Crypto\Exception\BadFormatException
	 * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
	 */
    public static function set_instance_api_key($value)
    {
        EncryptionHelper::add_option(CE_INSTANCE_API_KEY_KEY, $value);
    }

    /**
     * Gets a string representing all the plugins that were activated for synchronization during the setup process.
     * @return string|array
     */
    public static function get_activated_plugins()
    {
        return get_option(CE_ACTIVATED_PLUGINS, array());
    }

    /**
     * Sets a string representing all the plugins that were activated for synchronization during the setup process.
     * @param $plugins
     */
    public static function set_activated_plugins($plugins)
    {
        update_option(CE_ACTIVATED_PLUGINS, $plugins);
    }

	/**
	 * Will clear all the registered options for this plugin.
	 * Only the Unique Id won't be cleared so that we can restore the link when the plugin is reactivated.
	 *
	 * @param $clear_all bool When set to 'true' the instance UUID will be re-generated, this will cause the link between the plugin and the user account to break.
	 */
    public static function clear_options($clear_all)
    {
        delete_option(CE_INSTANCE_ID_KEY);
        delete_option(CE_INSTANCE_API_KEY_KEY);
        delete_option(CE_CONNECTED_ACCOUNT_ID);
        delete_option(CE_ACTIVATED_PLUGINS);
        
        if($clear_all === true) {
            delete_option(CE_INSTANCE_UUID_KEY);
            delete_option(CE_ENCRYPTION_KEY_KEY);
        }
    }
}
<?php


namespace CreativeMail\Modules\WooCommerce\Models;

use WC_Countries;

class WCStoreInformation
{
  public $address1;
  public $address2;
  public $city;
  public $postcode;
  public $state;
  public $country;
  public $currency;
  public $currency_symbol;
  
  function __construct()
  {
    if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))))
    {
        $location = wc_get_base_location();
        $this->address1 = apply_filters( 'woocommerce_countries_base_address', get_option( 'woocommerce_store_address', '' ));
        $this->address2 = apply_filters( 'woocommerce_countries_base_address_2', get_option( 'woocommerce_store_address_2', '' ));
        $this->city     = apply_filters( 'woocommerce_countries_base_city',  $location);
        $this->postcode = apply_filters( 'woocommerce_countries_base_postcode', $location );
        $this->state    = apply_filters( 'woocommerce_countries_base_country', $location );
        $this->country  = apply_filters( 'woocommerce_countries_base_country', $location );
        $this->currency_symbol = get_woocommerce_currency_symbol();
        $this->currency = get_woocommerce_currency();
    }
  }
}
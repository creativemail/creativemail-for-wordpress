<?php

namespace CreativeMail\Modules\Contacts\Handlers;

define('NL_EventType', 'WordPress - NewsLetter');

use CreativeMail\Modules\Contacts\Models\ContactModel;
use CreativeMail\Modules\Contacts\Models\OptActionBy;

class NewsLetterContactFormPluginHandler extends BaseContactFormPluginHandler
{
    public function convertToContactModel($user)
    {
        $contactModel = new ContactModel();
  
        $contactModel->setEventType(NL_EventType);
        $contactModel->setOptIn(true);
        $contactModel->setOptActionBy(OptActionBy::Visitor);
    
        $email = $user->email;
        if ($this->isNotNullOrEmpty($email)) {
          $contactModel->setEmail($email);
        }
    
        $name = $user->name;
        if ($this->isNotNullOrEmpty($name)) {
          $contactModel->setFirstName($name);
        }
    
        $surname = $user->surname;
        if ($this->isNotNullOrEmpty($surname)) {
          $contactModel->setLastName($surname);
        }
        
        return $contactModel;
    }
    
    public function ceHandleContactNewsletterSubmit($user) {
      try {
        $this->upsertContact($this->convertToContactModel($user));
      }
      catch (\Exception $exception) {
        // silent exception
      }
    }

    public function registerHooks()
    {
        add_action( 'newsletter_user_confirmed', array($this, 'ceHandleContactNewsletterSubmit'));
        // add hook function to synchronize
        add_action(CE_SYNCHRONIZE_ACTION, array($this, 'syncAction'));
    }

    public function unregisterHooks()
    {
        remove_action( 'newsletter_user_confirmed', array($this, 'ceHandleContactNewsletterSubmit'));
        // remove hook function to synchronize
        remove_action(CE_SYNCHRONIZE_ACTION, array($this, 'syncAction'));
    }

    public function syncAction()
    {
        // select * from wp_newsletter;
        global $wpdb;
        $result = $wpdb->get_results('select * from wp_newsletter where 1=1 order by id desc');

        $backfillArray = array();

        if (isset($result) && !empty($result)) {
            foreach ($result as $contact) {
                $contactModel = new ContactModel();
                $contactModel->setEventType(NL_EventType);
                $contactModel->setOptIn(true);
                $contactModel->setOptActionBy(OptActionBy::Visitor);

                $email = $contact->email;
                if ($this->isNotNullOrEmpty($email)) {
                    $contactModel->setEmail($email);
                }

                $name = $contact->name;
                if ($this->isNotNullOrEmpty($name)) {
                    $contactModel->setFirstName($name);
                }

                $surname = $contact->surname;
                if ($this->isNotNullOrEmpty($surname)) {
                    $contactModel->setLastName($surname);
                }

                if ($this->isNotNullOrEmpty($contactModel->getEmail())) {
                    array_push($backfillArray, $contactModel);
                }
            }
        }

        if (!empty($backfillArray)) {
            $this->batchUpsertContacts($backfillArray);
        }
    }

    function __construct()
    {
        parent::__construct();
    }
}
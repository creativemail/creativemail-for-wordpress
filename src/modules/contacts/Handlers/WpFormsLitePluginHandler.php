<?php


namespace CreativeMail\Modules\Contacts\Handlers;

define('WPF_EventType', 'WordPress - WPFormsLite');

use CreativeMail\Modules\Contacts\Models\ContactModel;
use CreativeMail\Modules\Contacts\Models\OptActionBy;

class WpFormsLitePluginHandler extends BaseContactFormPluginHandler
{
  private function get_form_type_field($formData, $type)
  {
    foreach ($formData as $field) {
      if(array_key_exists('type', $field) && $field['type'] === $type) {
        return $field;
      }
    }
    return null;
  }
  
  public function convertToContactModel($formData)
  {
    $contactModel = new ContactModel();
    
    $contactModel->setEventType(WPF_EventType);
    $contactModel->setOptIn(true);
    $contactModel->setOptActionBy(OptActionBy::Visitor);
    
    $emailField = $this->get_form_type_field($formData, 'email');
    if (array_key_exists('value', $emailField)) {
      if ($this->isNotNullOrEmpty($emailField['value'])) {
        $contactModel->setEmail($emailField['value']);
      }
    }
    
    $nameField = $this->get_form_type_field($formData, 'name');
    if (array_key_exists('first', $nameField)) {
      if ($this->isNotNullOrEmpty($nameField['first'])) {
        $contactModel->setFirstName($nameField['first']);
      }
    }
    if (array_key_exists('last', $nameField)) {
      if ($this->isNotNullOrEmpty($nameField['last'])) {
        $contactModel->setLastName($nameField['last']);
      }
    }
    
    return $contactModel;
  }
  
  public function ceHandleWpFormsProcessComplete($fields, $entry, $form_data, $entry_id) {
    try {
      $this->upsertContact($this->convertToContactModel($fields));
    }
    catch (\Exception $exception) {
      // silent exception
    }
  }
  
  public function registerHooks()
  {
    // https://wpforms.com/developers/wpforms_process_complete/
    add_action( 'wpforms_process_complete', array($this, 'ceHandleWpFormsProcessComplete'), 10, 4);
  }
  
  public function unregisterHooks()
  {
    remove_action( 'wpforms_process_complete', array($this, 'ceHandleWpFormsProcessComplete'));
  }
  
  public function syncAction()
  {
  
  }
  
  function __construct()
  {
    parent::__construct();
  }
}
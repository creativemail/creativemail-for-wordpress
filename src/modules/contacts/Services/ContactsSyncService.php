<?php

namespace CreativeMail\Modules\Contacts\Services;

use CreativeMail\Helpers\EnvironmentHelper;
use CreativeMail\Helpers\OptionsHelper;
use CreativeMail\Modules\Contacts\Models\ContactModel;
use Exception;

class ContactsSyncService
{
    private $apiKey;
    private $baseUrl;
    private $accountId;

    private function validate_email_address($emailAddress) {
        if (!isset($emailAddress) && empty($emailAddress)) {
            throw new Exception('No valid email address provided');
        }
    }

    private function ensure_event_type($eventType) {
        // DEV: For now, we only support WordPress.
        if (isset($eventType) && !empty($eventType)) {
            return $eventType;
        }

        return 'WordPress';
    }

//    private function buildInstanceJwt() {
//        $payload = array(
//            'instanceId' => $this->instanceId,
//            'exp' => time() + 3600
//        );
//
//        return JWT::encode($payload, $this->apiSecret, 'HS256');
//    }

    private function build_payload($contactModels) {
        $contacts = array();
        foreach ($contactModels as $model) {
            array_push($contacts, $model->toArray());
        }

        $data = array(
            "contacts" => $contacts
        );

        return wp_json_encode($data);
    }

    private function send($httpMethod, $endpoint, $payload) {

        if (!isset($httpMethod) || empty($httpMethod)) {
            // throw exception
        }

        if (!isset($endpoint) || empty($endpoint)) {
            throw new Exception('No endpoint provided');
        }

        $httpMethod = strtoupper($httpMethod);
        
        if ($httpMethod === 'POST') {
            return wp_remote_post($endpoint, array(
              'method' => 'POST',
              'headers' => array(
                'x-account-id' => $this->accountId,
                'x-api-key' => $this->apiKey,
                'content-type' => 'application/json'
              ),
              'body' => $payload
            ));
        }
  
        return wp_remote_get($endpoint, array(
          'method' => 'GET',
          'headers' => array(
            'x-account-id' => $this->accountId,
            'x-api-key' => $this->apiKey,
            'content-type' => 'application/json'
          )
        ));
    }

    public function upsertContact(ContactModel $contactModel) {
        if(!isset($contactModel)) {
            return false;
        }

        $this->validate_email_address($contactModel->getEmail());
        $contactModel->setEventType($this->ensure_event_type($contactModel->getEventType()));

        $url =  $this->baseUrl . '/v1.0/contacts';
        $result = $this->send('POST', $url, $this->build_payload(array($contactModel)));
        
        return (isset($result) && !empty($result));
    }

    public function upsertContacts($contactModels) {

        $url = $this->baseUrl . '/v1.0/contacts';
        $jsonData = $this->build_payload($contactModels);
        $result = $this->send('POST', $url, $jsonData);

        return (isset($result));
    }

    function __construct() {
        $this->apiKey = OptionsHelper::get_instance_api_key();
        $this->accountId = OptionsHelper::get_connected_account_id();
        $this->baseUrl = EnvironmentHelper::get_app_gateway_url('wordpress');
    }
}

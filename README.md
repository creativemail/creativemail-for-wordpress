### Creativ.eMail by Constant Contact
- Tags: email, marketing, newsletter, subscribe, contact form 7, woocommerce, constant contacts
- Requires at least: 4.6
- Tested up to: 5.3
- Stable tag: 1.0.0
- License: GPLv2 or later
- License URI: http://www.gnu.org/licenses/gpl-2.0.html
- Requires PHP: 7.1

#### Description
Power your WooCommerce Store or WordPress Blog with simple & free email marketing from Constant Contact.
With the official Creativ.eMail for WooCommerce plugin, your products, blog posts, images and store links are automatically included as rich shoppable email marketing content for your customers. Our included CRM also intelligently pulls in and identifies your WordPress site contacts and WooCommerce store customers. That makes it easy to build audiences and send targeted customer campaigns. Get free email marketing, 98% deliverability, and Constant Contact rock solid reliability all without ever needing to leave your WP Admin.

#### Features
A Constant Contact WordPress plugin that gathers contacts submitted via the WordPress site forms and enables users to send email campaigns in a simple and intuitive way. 

The plugin offers the following functionality:

- Automated email creation
- Section based, offering preset vs unlimited design options
- Seamless integration with the following Contact opt-ins without need for adding JMML:
    - Contact Form 7
    - WooCommerce
    - WPForms Lite
    - Newsletter
- Combined Contacts CRM (fed from WordPress Site forms & WooCommerce store)
- Stock images (Unsplash integration, for free)
- WordPress Blog Posts & WooCommerce Products integration to allow easy sharing via email campaigns


#### Installing the plugin
1. In your WordPress admin panel, go to *Plugins > New Plugin*, search for `Creativ.eMail by Constant Contact` and click "*Install now*"
1. Alternatively, download the plugin and upload the contents of `*.zip` to your plugins directory, which usually is `/wp-content/plugins/`.
1. Activate the plugin
1. Log Into your account or sign up.


